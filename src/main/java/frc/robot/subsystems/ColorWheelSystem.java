/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import javax.swing.colorchooser.ColorChooserComponentFactory;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Add your docs here.
 */
public class ColorWheelSystem extends SubsystemBase {

  public int colorData = 0; //stores color in integer form from the FRC DriverStation
  
  //private final TalonSRX colorMotor = new TalonSRX(Constants.COLOR_MOTOR);  //creates color motor
  
  private final VictorSP colorMotor = new VictorSP(Constants.COLOR_MOTOR);

  //
  private final I2C.Port i2cPort = I2C.Port.kOnboard;  //configures I2C port on RoboRIO for the color sensor
  
  private final ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);

  private final ColorMatch m_colorMatcher = new ColorMatch();

  private final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);  //Defines blue color

  private final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);  //Defines green color

  private final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);  //Defines red color

  private final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);  //Defines yellow color



  //@Override
  public void robotInit() {

    m_colorMatcher.addColorMatch(kBlueTarget);

    m_colorMatcher.addColorMatch(kGreenTarget);

    m_colorMatcher.addColorMatch(kRedTarget);

    m_colorMatcher.addColorMatch(kYellowTarget);    

  }

  @Override
  public void periodic() {
   /**

     * The method GetColor() returns a normalized color value from the sensor and can be

     * useful if outputting the color to an RGB LED or similar. To

     * read the raw color, use GetRawColor().

     * 

     * The color sensor works best when within a few inches from an object in

     * well lit conditions (the built in LED is a big help here!). The farther

     * an object is the more light from the surroundings will bleed into the 

     * measurements and make it difficult to accurately determine its color.

     */


    m_colorMatcher.addColorMatch(kBlueTarget);

    m_colorMatcher.addColorMatch(kGreenTarget);

    m_colorMatcher.addColorMatch(kRedTarget);

    m_colorMatcher.addColorMatch(kYellowTarget);   

    final Color detectedColor = m_colorSensor.getColor();



    /**

     * Run the color match algorithm on our detected color

     */

    String colorString;

    final ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);



    if (match.color == kBlueTarget) {

      colorString = "Blue";

    } else if (match.color == kRedTarget) {

      colorString = "Red";

    } else if (match.color == kGreenTarget) {

      colorString = "Green";

    } else if (match.color == kYellowTarget) {

      colorString = "Yellow";

    } else {

      colorString = "Unknown";

    }

  }
  public ColorWheelSystem() {
    //addChild("Color_Motlor", colorMotor);
    //addChild("Right Intake", rightIntake);
  }

 

  
  public void positionControll(final double speed) {
    //code for spinning wheel 3.5 revolutions
    //colorMotor.set(ControlMode.PercentOutput, speed);
    colorMotor.set(speed);
  }

  public void colorControll () {
    //colorMotor.set(ControlMode.PercentOutput, speed);
    final Color detectedColor = m_colorSensor.getColor();
    final ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);
    
    if(colorData == 1) {
      //code
      SmartDashboard.putNumber("Game Color", 1);
      SmartDashboard.putString("Game Color", "Blue");

      if(match.color != kBlueTarget) {
        //colorMotor.set(ControlMode.PercentOutput, speed);
        //colorMotor.set(ControlMode.PercentOutput, 0.8);
        colorMotor.set(0.8);
        SmartDashboard.putString("Status", "ColorSet: inProgress");
      }
      else if(match.color == kBlueTarget) {
        //colorMotor.set(ControlMode.PercentOutput, 0);
        colorMotor.set(0);
        SmartDashboard.putString("Status", "ColorSet: Completed");
      }
      //else if(colorString == "Blue");

    }
    else if(getColorData() == 2) {
      //code
      SmartDashboard.putNumber("Game Color", 2);
      SmartDashboard.putString("Game Color", "Green");

      if(match.color != kGreenTarget) {
        //colorMotor.set(ControlMode.PercentOutput, speed);
        //colorMotor.set(ControlMode.PercentOutput, 0.8);
        colorMotor.set(0.8);
        SmartDashboard.putString("Status", "ColorSet: inProgress");
      }
      else if(match.color == kGreenTarget) {
        //colorMotor.set(ControlMode.PercentOutput, 0);
        colorMotor.set(0);
        SmartDashboard.putString("Status", "ColorSet: Completed");
      }

    }
    else if(colorData == 3) {
      //code
      SmartDashboard.putNumber("Game Color", 3);
      SmartDashboard.putString("Game Color", "Red");

      if(match.color != kRedTarget) {
        //colorMotor.set(ControlMode.PercentOutput, speed);
        //colorMotor.set(ControlMode.PercentOutput, 0.8);
        colorMotor.set(0.8);
        SmartDashboard.putString("Status", "ColorSet: inProgress");
      }
      else if(match.color == kRedTarget) {
        //colorMotor.set(ControlMode.PercentOutput, 0);
        colorMotor.set(0);
        SmartDashboard.putString("Status", "ColorSet: Completed");
      }
    }
    else if(colorData == 4) {
      //code
      SmartDashboard.putNumber("Game Color", 4);
      SmartDashboard.putString("Game Color", "Yellow");

      if(match.color != kYellowTarget) {
        //colorMotor.set(ControlMode.PercentOutput, speed);
        //colorMotor.set(ControlMode.PercentOutput, 0.8);
        colorMotor.set(0.8);
        SmartDashboard.putString("Status", "ColorSet: inProgress");
      }
      else if(match.color == kYellowTarget) {
        //colorMotor.set(ControlMode.PercentOutput, 0);
        colorMotor.set(0);
        SmartDashboard.putString("Status", "ColorSet: Completed");
      }
    }
  }

  public int getColorData() {
    String gameData;
    var colorData = 0;
    gameData = DriverStation.getInstance().getGameSpecificMessage();
    if(gameData.length() > 0) {
      switch (gameData.charAt(0)) {
        case 'B' :
        //Blue case code
        colorData = 1;
        break;
        case 'G' :
        //Green case code
        colorData = 2;
        break;
        case 'R' :
        //Red case code
        colorData = 3;
        break;
        case 'Y' :
        //Yellow case code
        colorData = 4;
        break;
      }
    }
    else {
      //Code for no data recived yet
      colorData = 3;
      colorMotor.set(1);
      //SmartDashboard.putString("Status", "NoData");
    }
    return colorData;
  }

}
